#!/bin/bash

# strip latex formatting out of string defns
sed -e 's/.ensuremath....textit{\(.*\)}}}/\1/' strings-long.bib > .resolve.bib

# copy in Derek's papers
gawk '
    BEGIN { flag = 0; }
    /BEGIN: DEREK/ { flag = 1; }
    flag { print; }
    /END: DEREK/ { flag = 0; }' \
    derek.bib | sed \
        -e 's/\\SAG{\(\w*\)}/\1/g' \
        -e 's/\\SA{\(\w*\)}/\1/g' \
        -e 's/{\\SAG.H.-Christian}}/H.-Christian/g' \
        -e 's/\\SAG{P.}/P./g' \
        -e 's/\\SAG{Ming-Ho}/Ming-Ho/g' \
        -e 's/\\SA{\(B.*\)}/\1/' \
    >> .resolve.bib

# copy in proceedings records
cat proceedings.bib >> .resolve.bib

# resolve cross-references and strings
./resolve.rb .resolve.bib > resolved_ccv.bib


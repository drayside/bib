#!/usr/bin/ruby
# use bibtex-ruby to resolve cross-references and string constants

# check command line argument
if ARGV.empty? then
    puts "please specify a bib file to resolve"
    exit
else
    bib=ARGV[0]
end


# useful number test
# http://stackoverflow.com/questions/8616360/how-to-check-if-a-variable-is-a-number-or-a-string
class Object
  def is_number?
    self.to_f.to_s == self.to_s || self.to_i.to_s == self.to_s
  end
end


# main script
require 'bibtex'
b = BibTeX.open(bib)
b.replace.join

# types of entries to print
# do not print proceedings entries (resolved into inproceedings)
types = SortedSet.new([
    "article",
    "book",
    "incollection",
    "inproceedings",
    "techreport",
    ])
b.each{ |e| 
    ets = e.type.to_s
    if types.include?(ets) then
        puts "@" + e.type.to_s + "{" + e.key.to_s + ","
        e.field_names.each { |f, v| 
            fs = f.to_s
            vs = e[f].to_s
            if vs.is_number? or fs == "month" then
                # year or month should not be quoted
                puts "    " + fs + " = " + vs + ","
            else
                # most string values should be quoted
                puts "    " + fs + " = {" + vs + "},"
            end
            }
        puts "}"
        puts ""
    end
}

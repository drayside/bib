#!/bin/bash

# strip latex formatting out of string defns
sed -e 's/.ensuremath....textit{\(.*\)}}}/\1/' strings-long.bib > .resolve.bib

# copy in Derek's bibliography
cat derek.bib >> .resolve.bib

# copy in proceedings records
cat proceedings.bib >> .resolve.bib

# resolve cross-references and strings
./resolve.rb .resolve.bib > resolved.bib

